package com.kowsar.springsecurityjwt.Service;

import com.kowsar.springsecurityjwt.model.ResponseStatus;
import com.kowsar.springsecurityjwt.request.AuthenticationRequest;
import com.kowsar.springsecurityjwt.response.ResponseDto;
import com.kowsar.springsecurityjwt.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Slf4j
@Service
public class AuthenticationService implements Serializable {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    @Autowired
    JwtUtil jwtUtil;

    public ResponseEntity<ResponseDto> authenticate(AuthenticationRequest request) throws Exception{
        UserDetails userDetails = null;
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
            userDetails = customUserDetailsService.loadUserByUsername(request.getUsername());
        } catch (BadCredentialsException ex) {
            log.error("Wrong username or password", ex);
            throw new Exception("Wrong username or password", ex);
        }catch (Exception ex){
            log.error("An Exception occurred authenticaiton", ex);
            throw new Exception("Wrong username or password", ex);
        }
        String jwtToken = jwtUtil.generateToken(userDetails);
        return new ResponseEntity<>(
                ResponseDto.builder()
                        .responseMessage("SUCCESS")
                        .responseStatus(ResponseStatus.OK)
                        .data(jwtToken)
                        .build(),
                HttpStatus.OK
        );
    }

}
