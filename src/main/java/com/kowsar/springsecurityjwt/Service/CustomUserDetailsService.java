package com.kowsar.springsecurityjwt.Service;

import com.kowsar.springsecurityjwt.model.CustomUserDetails;
import com.kowsar.springsecurityjwt.model.User;
import com.kowsar.springsecurityjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(username);

        user.orElseThrow(() -> new UsernameNotFoundException(String.format("User not found %s", username)));

        return user.map(CustomUserDetails::new).get();
    }

}
