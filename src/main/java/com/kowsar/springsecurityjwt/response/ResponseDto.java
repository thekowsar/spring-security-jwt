package com.kowsar.springsecurityjwt.response;

import com.kowsar.springsecurityjwt.model.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDto<T> implements Serializable {

    private String responseMessage;

    private ResponseStatus responseStatus;

    private T data;

}
