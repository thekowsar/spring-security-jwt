package com.kowsar.springsecurityjwt.controller;

import com.kowsar.springsecurityjwt.Service.AuthenticationService;
import com.kowsar.springsecurityjwt.Service.CustomUserDetailsService;
import com.kowsar.springsecurityjwt.model.ResponseStatus;
import com.kowsar.springsecurityjwt.request.AuthenticationRequest;
import com.kowsar.springsecurityjwt.response.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.kowsar.springsecurityjwt.constants.ControllerConstants.*;

@Slf4j
@RestController
public class HomeController {

    @Autowired
    AuthenticationService authenticationService;

    @PostMapping(value = AUTHENTICATE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> authenticate(@RequestBody AuthenticationRequest request) throws Exception {
        log.info("Request received {} for {}", AUTHENTICATE_URL, request);
        ResponseEntity response = authenticationService.authenticate(request);
        log.info("Response returned {} for {}", AUTHENTICATE_URL, response);
        return response;
    }

    @GetMapping(value = HOME_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> home(){
        log.info("Request received for {}", HOME_URL);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseMessage("SUCCESS")
                        .responseStatus(ResponseStatus.OK)
                        .data("WELCOME HOME")
                        .build(),
                HttpStatus.OK
        );
        log.info("Response return {} for {}", HOME_URL, response);
        return response;
    }

    @GetMapping(value = USER_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> user(){
        log.info("Request received for {}", USER_URL);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseMessage("SUCCESS")
                        .responseStatus(ResponseStatus.OK)
                        .data("WELCOME USER")
                        .build(),
                HttpStatus.OK
        );
        log.info("Response return {} for {}", USER_URL, response);
        return response;
    }

    @GetMapping(value = ADMIN_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> admin(){
        log.info("Request received for {}", ADMIN_URL);
        ResponseEntity response = new ResponseEntity(
                ResponseDto.builder()
                        .responseMessage("SUCCESS")
                        .responseStatus(ResponseStatus.OK)
                        .data("WELCOME ADMIN")
                        .build(),
                HttpStatus.OK
        );
        log.info("Response return {} for {}", ADMIN_URL, response);
        return response;
    }

}
