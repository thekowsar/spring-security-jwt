package com.kowsar.springsecurityjwt.model;

public enum ResponseStatus {

    OK,
    FAILED,
    CREATED,
    UPDATED,
    DELETED,
    NOT_FOUND

}
