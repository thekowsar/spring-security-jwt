package com.kowsar.springsecurityjwt.constants;

public interface ControllerConstants {

    String HOME_URL = "/";
    String USER_URL = "/user";
    String ADMIN_URL = "/admin";
    String AUTHENTICATE_URL = "/authenticate";

}
